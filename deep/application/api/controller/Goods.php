<?php

namespace app\api\controller;
use \think\Db;

use app\api\model\Database;


class Goods
{
   public function my(){
       
        $db=Db::connect('tao');
        
   }
    
   public function addGoods(){
          $gallery  = input('gallery', '', 'trim');
          $goodsDesc  = input('goodsDesc', '', 'trim');
          $goodsType = input('goodsType', '', 'trim');
          $img = input('img', '', 'trim');
          $openid  = input('openid', '', 'trim');
          $price =  input('price', '', 'trim');
          $schoolId =input('schoolId', '', 'trim');
          $title =  input('title', '', 'trim');
          $contact =  input('tel', '', 'trim');
          
              $time = time();
              $nowtime = date("Y-m-d H:i:s",$time);
           $data = [
           'title'=>$title,
           'gallery' =>$gallery,
           'goods_desc' =>$goodsDesc,
           'create_time'=>$nowtime,
           'goods_type' =>$goodsType,
           'img' =>$img,
           'openid'=>$openid,
           'price'=>$price,
           'school_id'=>$schoolId,
           '_id'=>$time,
           'contact'=>$contact
             ];
           // ajax_return_ok($data,'err'); 
            $db=Db::connect('tao');
            $is= $db->name('goods')->insert($data);
            if($is){
                 
                 ajax_return_ok($is,'success'); 
            }else{
                 ajax_return_ok($is,'err'); 
            }
           
         
   }
  
   public function getGoodsById(){
        $id  = input('id', '', 'trim');
        if($id==0){
            ajax_return_ok(0,'err'); 
        }
        $db=Db::connect('tao');
        $result=$db->name('goods')->
        alias('a') ->
        join('wx_user w','a.openid=w.openid')->
        where('goods.id',$id)->
        find();
         
        if($result){
             ajax_return_ok($result, 'success' );
        }
        else{
            ajax_return_ok($result,'err'); 
        }
     
    }
 
   public function getPageList(){
                $db=Db::connect('tao');
                $asc  = input('asc', '', 'trim');//是否正序
                $current  = input('current', '', 'trim');//第几页
                $goodsType  = input('goodsType', '', 'trim');//商品类型
                $lprice  = input('lprice', '', 'trim');//最低价格
                $orderByName  = input('orderByName', '', 'trim');//排序字段 create_time default price
                $priceRange  = input('priceRange', '', 'trim');//价格区间 布尔
                $rangeName  = input('rangeName', '', 'trim');//排序名称
                $rprice  = input('rprice', '', 'trim');//最高价格
                $schoolId  = input('schoolId', '', 'trim');
                $size  = input('size', '', 'trim');//每页数量
                $key  = input('key', '', 'trim');
                $like  = input('like', '', 'trim');
                $isase ='asc';
            if(!$asc)$isase = 'desc';
            if($orderByName=='default')$orderByName  ='create_time';
            if($key){
                         $list=$db->name('goods')->
                 alias('a') ->
             join('wx_user w','a.openid=w.openid')->
              where('is_sale','=',0)->
              where('title','like','%'.$key.'%')->
               whereor('goods_desc','like','%'.$key.'%')->
             order('goods.'.$orderByName.' '.$isase)->
             paginate($size,false,['page'=>$current]);
             ajax_return_ok($list,'success');
                    }
                    
           if($goodsType==0){
             $list=$db->name('goods')->
             alias('a') ->
             join('wx_user w','a.openid=w.openid')->
             where('is_sale','=',0)->
             order('goods.'.$orderByName.' '.$isase)->
             paginate($size,false,['page'=>$current]);
             ajax_return_ok($list,'success');
            }
           if($priceRange){
           
           $list=$db->name('goods')->alias('a') ->
           join('wx_user w','a.openid=w.openid')->
           where('goods_type','=',$goodsType)->
           where('is_sale','=',0)->
           where('price','>=',$lprice)->
           where('price','<=',$rprice)->
           paginate($size,false,['page'=>$current]);
           }else if(!$priceRange){
               
           $list=$db->name('goods')->alias('a') ->
           join('wx_user w','a.openid=w.openid')->
           where('goods_type','=',$goodsType)->
           where('is_sale','=',0)->
           order('goods.'.$orderByName.' '.$isase)->
           paginate($size,false,['page'=>$current]);
           ajax_return_ok($list,'13213');
           
           }else {
           $list=$db->name('goods')->alias('a') ->
           where('goods_type','=',$goodsType)->
           where('is_sale','=',0)->
           join('wx_user w','a.openid=w.openid')->
           paginate($size,false,['page'=>$current]);
           }
          
        //   $user = Db::name('user')->where('openId',$list['openid'])->find();
        //   $t_user = array("user"=>$user);order($orderByName.' '.$isase)->
           
          if($list){
             ajax_return_ok($list,'success');
            }
        else{
            ajax_return_ok($list,'err'); 
        }
     
    }
  
   public function deleteById(){
          $id  = input('id', '', 'trim');
          $openid  = input('openid', '', 'trim');
          $db=Db::connect('tao');
          $id = $db->name('goods')->
          where('id','=',$id)->
          field('id,openid')->
          find();
          if($id['id']&&$id['openid']){
             $is= $db->name('goods')->
          where('id','=',$id['id'])->
          where('openid','=',$id['openid'])->
          delete();
              ajax_return_ok($is ,'success');
              
          }else{
              ajax_return_ok($id,'id is fail');
          }
   }
   
   public function getMessagePage(){
       $current  = input('current', '', 'trim');
       $goodsId = input('goodsId', '', 'trim');
       $size  = input('size', '', 'trim');
       $db=Db::connect('tao');
      $list =  $db->name('goods_message')->
        alias('a') ->
        join('wx_user w','user_id=wxid')->
        where('goods_id','=',$goodsId)->
        where('first_id','=',0)->
        where('parent_id','=',0)->
        order('goods_message.create_time desc')->
        select();
        $i = 0;
        while($i<count($list)){
            $item=$db->name('goods_message')->
             alias('a') ->
           join('wx_user w','a.user_id=w.wxid')->
           where('goods_id','=',$goodsId)->
           where('first_id','=',$list[$i]['id'])->
            order('goods_message.create_time desc')->
           select(); 
           $replyMessage =  array('replyMessage'=>$item);
           $list[$i] = $list[$i]+$replyMessage;
            $i++;
        }
     
       ajax_return_ok($list,count($db->name('goods_message')-> where('goods_id','=',$goodsId)->select()));
       
   }
    
   public function addComment(){
         $message  = input('message', '', 'trim');
          $user_id  = input('user_id', '', 'trim');
          $goods_id  = input('goods_id', '', 'trim');
          $parent_id  = input('parent_id', '', 'trim');
           $first_id  = input('first_id', '', 'trim');
              $time = time();
              $nowtime = date("Y-m-d H:i:s",$time);
              $db=Db::connect('tao');
             $data = ['message'=>$message,
             'user_id'=>$user_id,
             'goods_id'=>$goods_id,
             'parent_id'=>$parent_id,
             'first_id'=>$first_id,
             'create_time'=>$nowtime
             ];
            $db->name('goods_message')->insert($data); 
         ajax_return_ok($data,'success');
       
   }
    
   public function deleteByIdM(){
          $id  = input('id', '', 'trim');
          $user_id  = input('user_id', '', 'trim');
          $db=Db::connect('tao');
          $id = $db->name('goods_message')->
          where('id','=',$id)->
          field('id,user_id')->
          find();
          if($id['id']&&$id['user_id']){
             $is= $db->name('goods_message')->
          where('id','=',$id['id'])->
          where('user_id','=',$id['user_id'])->
          delete();
              ajax_return_ok($is ,'success');
              
          }else{
              ajax_return_ok($id,'id is fail');
          }
   }
   
   public function getGoodsType(){
        $db=Db::connect('tao');
        $list = $db->name('goods_second_type')->select();
        $temp;
        $i = 0;
        while($i<count($list)){
            $temp[$i]['id'] = $i;
            $temp[$i]['text'] = $list[$i]['second_name'];
            $temp[$i]['goodsType'] = $list[$i]['id'];
            $i++;
        }
        ajax_return_ok($temp,'success');
   }
   
   public function getLikeinfo(){
       $openid = input('openid','','trim');
         $db=Db::connect('tao');
       $list = $db->name('goods_collect')->
       join('goods','goods.id = goods_collect.goods_id')->
       where('goods_collect.openid','=',$openid)->
       join('wx_user b','goods.openid=b.openid')->
       select();
       if($list){
           ajax_return_ok($list,'success');//
       }else{
           ajax_return_ok($list,'err');//
       }
       
       
   }
   
   public function LikeGoods(){
       $openid = input('openid','','trim');
       $goods_id = input('goods_id','','trim');
       $db=Db::connect('tao');
       $temp = $db->name('goods_collect')->
       where('goods_id','=',$goods_id)->
        where('openid','=',$openid)->
        find();
        //ajax_return_ok($openid,$goods_id);
       if($openid&&$goods_id){
           $data = [
               'openid'=>$openid,
               'goods_id'=>$goods_id
               ];
               if($temp){
                   ajax_return_ok(0,'is like');
               }else{
                   $result=  $db->name('goods_collect')->insert($data);
                   ajax_return_ok($result,'success');
               }
             
       }
       else{
           ajax_return_ok(0,'openid or goods_id is null');
       }
     }
     
   public function myGoods(){
        $current  = input('current', '', 'trim');
         $openid  = input('openid', '', 'trim');
         $is_sale  = input('is_sale', '', 'trim');
        // $current = 0;
          $db = Db::connect('tao');
          if($is_sale==0||$is_sale==1){
            $list = $db->name('goods')->
            alias('a') ->
               join('wx_user w','a.openid = w.openid')->
          where('goods.openid','=',$openid)->
          where('goods.is_sale','=',$is_sale)->
          paginate(10,false,['page'=>$current]);
          }
         else{
              $list = $db->name('goods')->
              alias('a') ->
               join('wx_user w','a.openid = w.openid')->
          where('goods.openid','=',$openid)->
          paginate(10,false,['page'=>$current]);
         }
        
        
          if($list){
              ajax_return_ok($list,'success');
          }else{
              ajax_return_ok($list,'err');
          }
   }
   
   public function getGoodsPrise(){
       $id = input('id','','trim');
       $db = Db::connect('tao');
      $item= $db->name('goods')->
       where('id','=',$id)->
       field('contact,visit_num')->find();
       if($item){
           ajax_return_ok($item,'success');
       }else{
             ajax_return_ok($item,'fail');
       }
   }
  
   public function addWant(){
      $id = input('id','','trim');
      $db = Db::connect('tao');
      $item= $db->name('goods')->
      where('id','=',$id)->
      setInc('visit_num',1);
       if($item){
           ajax_return_ok($item,'success');
       }else{
             ajax_return_ok($item,'fail');
       }
    }
    
   public function change_sale(){
        $openid = input('openid','','trim');
        $goods_id = input('goods_id','','trim');
        $sale = input('sale','','trim');
        $db = Db::connect('tao');
        $temp= $db->name('goods')->
        where('id','=',$goods_id)->
        where('openid','=',$openid)->
        update(['is_sale'	=>	$sale]);
        ajax_return_ok($temp,'status');
        
   }
}