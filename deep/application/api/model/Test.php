<?php


namespace app\api\model;
use \think\Model;

class Test extends Model
{
    public function usera(){
        return $this ->hasMany('user','openId','a');
    }
    public function userb(){
        return $this ->hasMany('user','openId','b');
    }

    public function topics()
    {
        return $this->hasMany('topics','id','tid');
    }
}