<?php


namespace app\admin\controller;
use \think\Db;

class User
{
	
		public function test(){
	
		 return 1;
	}
	
	public function user(){
		
		 $openid = input('z', '', 'trim');
		 if($openid){
		 	ajax_return_ok($openid);
		 }
		 else{
		 		ajax_return_ok("789");
		 }
		 
	}
	
    public function getAll(){
        ajax_return_ok(Db::name('user')->select());
    }
    
    public function addU(){
         $openid = input('openid', '', 'trim');
          $name = input('name', '', 'trim');
          $img = input('img', '', 'trim');
          $sex = input('sex', '', 'trim');
          $major = input('major', '', 'trim');
          $mac = input('mac', '', 'trim');
          $wx = input('wx', '', 'trim');
          $qq = input('qq', '', 'trim');
          $data = ['openid' =>$openid,'img' =>$img,'sex' =>$sex,'address' => $mac,'major'=> $major,'name'=> $name,'wx'=> $wx,'qq'=> $qq];
        if(!Db::name('info')->where('openid',$openid)->find()){
            
            Db::name('info')->insert($data);
            ajax_return_ok("ADD");
        }else{
             Db::name('info')->where('openid',$openid)->update($data);
           ajax_return_ok("UPDATA");
        }
    }
    
     public function addSMS(){
         
          $phone = input('phone', '', 'trim');
          $txt = input('txt', '', 'trim');
          $type = input('type ', '', 'trim');
         
         $data = ['phone' =>$phone,'txt' =>$txt,'type' =>$type];
          Db::name('sms')->insert($data);
       
        
    }
    
    
    
     public function mac(){
         $openid = input('openid', '', 'trim');
          $address = input('address', '', 'trim');
        
         ajax_return_ok(  Db::name('info')->where('openid',$openid)->update(['address' => $address]));
           
    }
    
     public function macToUser(){
        
          $address = input('address', '', 'trim');
        
          ajax_return_ok( Db::name('info')->where('address',$address)->find());
           
    }

    public function deleteByOpenid(){
      $openid = input('openid', '', 'trim');
      ajax_return_ok(Db::name('user')->where('openId',$openid)->delete());
    }

    public function getByOpenid(){
        $openid = input('openid', '', 'trim');
        
        ajax_return_ok(Db::name('user')->where('openId',$openid)->find(),"ok");
    }
    public function userChange(){
        $openid = input('openid', '', 'trim');
        $userName = input('userName', '', 'trim');
        $img = input('img', '', 'trim');
        $major = input('major', '', 'trim');
        $sign = input('sign', '', 'trim');
        $home = input('home', '', 'trim');
//        if(Db::name('user')->where('userName',$userName)->find()){
//            ajax_return_ok("userNamehaved");
//        }else{
            Db::name('action')->where('a',$openid)->update(['aName' => $userName]);
            Db::name('action')->where('b',$openid)->update(['bName' => $userName]);
            ajax_return_ok(Db::name('user')->where('openId',$openid)->update(['userName' => $userName,'home' => $home,'img' => $img,'sign' => $sign,"major" => $major]));
//        }

    }

    public function userChangeWithoutName(){
        $openid = input('openid', '', 'trim');
        $img = input('img', '', 'trim');
        $sign = input('sign', '', 'trim');
        $home = input('home', '', 'trim');

        ajax_return_ok(Db::name('user')->where('openId',$openid)->update(['home' => $home,'img' => $img,'sign' => $sign]));


    }

    public function  ban(){
         $openid = input('openid', '', 'trim');
         ajax_return_ok(Db::name('user')->where('openId', $openid)->update(['isEnabled' => 1]));
    }

    public function  isban(){
        $openid = input('id', '', 'trim');
        ajax_return_ok(Db::name('user')->where('openId', $openid)->field('isEnabled')->find());
    }

    public function  getHome(){
        $id = input('id', '', 'trim');
        ajax_return_ok(\app\admin\model\User::with('topics')->where('openId',$id)->select());
    }
}