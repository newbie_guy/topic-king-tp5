<?php


namespace app\admin\controller;

use const http\Client\Curl\Features\IDN;
use \think\Db;
class Equipment
{
    public function getAll(){
//        $a  = input('a', '', 'trim');
//        $b  = input('b', '', 'trim');
        $out  = Db::name('equipment')->where("id",0)->find();

        ajax_return_ok($out);

    }


    public function setws()
    {
        $a  = input('wendu', '', 'trim');
        $b  = input('shidu', '', 'trim');
        $data = ['wd' =>(int)($a),'sd' =>(int)($b)];
        $out  = Db::name('equipment')->where("id" ,0)->update($data);

        ajax_return_ok($out);


    }


    public function Light()
    {

        if((Db::name('equipment')->where("id" ,0)->find()["i1"]) == 0){
            $data = ['i1' =>1];
        }else{
            $data = ['i1' =>0];
        }
        $out  = Db::name('equipment')->where("id" ,0)->update($data);


    }
    public function wind()
    {
        if((Db::name('equipment')->where("id" ,0)->find()["i2"] )== 0){
            $data = ['i2' =>1];
        }else{
         $data = ['i2' =>0];
        }

        $out  = Db::name('equipment')->where("id" ,0)->update($data);
        ajax_return_ok($out);

    }
    public function getByOpenid()
    {



    }


    public function getAllStorage(){
//        $a  = input('a', '', 'trim');
//        $b  = input('b', '', 'trim');
        $out  = Db::name('cang')->select();

        ajax_return_ok($out);

    }

    public function open()
    {

        $id  = input('id', '', 'trim');
        $img  = input('img', '', 'trim');
        $data = ['state' =>1,"img" => $img];
        $out  = Db::name('cang')->where("id" ,$id)->update($data);

        ajax_return_ok($id);


    }

    public function close()
    {

        $id  = input('id', '', 'trim');

        $data = ['state' =>0,"img" => "","confirm"=>0];
        $out  = Db::name('cang')->where("id" ,$id)->update($data);

        ajax_return_ok($id);


    }

    public function setConfirm()
    {
        $id  = input('id', '', 'trim');

        $data = ['confirm' =>1];
        $out  = Db::name('cang')->where("id" ,$id)->update($data);
        ajax_return_ok($out);


    }

    public function getConfirm()
    {

        $out  = Db::name('cang')->where(["confirm"=>0,"state" => 1])->find();
        ajax_return_ok($out);


    }


}