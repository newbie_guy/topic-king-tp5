<?php


namespace app\admin\controller;

use \think\Db;
class Search
{
    public function user(){
        $search  = input('search', '', 'trim');
        $user =  Db::name('user')->where('userName', 'like', '%'.$search.'%')->select();
        $topic =\app\admin\model\Topic::with('user')->where('topic', 'like', '%'.$search.'%')->select();
        ajax_return_ok(compact('user','topic'));
    }

    public  function topic(){

    }

    public function topics(){

    }

}