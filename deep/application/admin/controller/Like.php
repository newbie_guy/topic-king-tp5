<?php


namespace app\admin\controller;

use \think\Db;
class Like
{
    public  function CODE($tid){
        $OUT = \app\admin\model\Topics::with('action,user,img,topic,like')->order('created_at desc')->where('id',$tid)->select();

        $A = $OUT;
        foreach ($OUT as $key => $item) {
            $A[$key]['content'] = base64_decode($item['content']);
            if(count($A[$key]['action'])){
                $info = $A[$key]['action'];//定义一个中间变量，操作这个变量，结果赋值给$list[$key]['info']
                foreach($info as $k =>$i){
                    $txt =base64_decode($i["txt"]);
                    $info[$k]["txt"] = $txt;
                }
                $A[$key]['action'] =$info;
            }

        }
        return $A;
    }

    public function add(){
        $a  = input('a', '', 'trim');
        $b  = input('b', '', 'trim');
        $tid  = input('tid', '', 'trim');
        $aName  = Db::name('user')->where('openId', $a)->find();
        $bName  = Db::name('user')->where('openId', $b)->find();

        $time  = date('y-m-d h:i:s',time());

        if( !Db::name('like')->where(['a' => $a,'b'=> $b])->find()){
            Db::name('user')->where('openId', $a)->setInc('like',1);
            Db::name('user')->where('openId', $b)->setInc('liked',1);
            $data = ['a' =>$a,'b' =>$b,'time' =>$time,'aName'=>$aName['userName'],'bName'=>$bName['userName'],'aImg'=>$aName['img'],'bImg'=>$bName['img']];
            Db::name('like')->insert($data);
            ajax_return_ok($this->CODE($tid));
        }else{
            Db::name('user')->where('openId', $a)->setDec('like',1);
            Db::name('user')->where('openId', $b)->setDec('liked',1);
            Db::name('like')->where(['a' => $a,'b'=> $b])->delete();
            ajax_return_ok($this->CODE($tid));
        }
    }

    public function getLikeAll(){
        $id  = input('id', '', 'trim');
        ajax_return_ok(Db::name('like')->alias('a')->
        join('user b','b.openId = a.b')->
        field('a.*,b.userName as bbname')->
        where('a', $id)->select());

    }

    public function getLikedAll(){
        $id  = input('id', '', 'trim');
        ajax_return_ok(Db::name('like')->
        alias('a')->
        join('user b','b.openId = a.a')->
        field('a.*,b.userName as aaname')->where('b', $id)->select());


    }

    public function jugeLike(){
        $a  = input('a', '', 'trim');
        $b  = input('b', '', 'trim');
        if(Db::name('like')->where(['a' => $a,'b' => $b])->find()){
            ajax_return_ok(true);
        }else{
            ajax_return_ok(false);
        }
        ajax_return_ok();


    }
}