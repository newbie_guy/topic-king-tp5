<?php


namespace app\admin\controller;

use app\admin\model\Com;
use \think\Db;
use AipImageCensor;

require_once 'AipImageCensor.php';
class Action
{
	
	 
	 
    public function addAction(){

    $a  = input('a', '', 'trim');
    $b  = input('b', '', 'trim');
    $aName  = Db::name('user')->where('openId', $a)->find();
    $bName  = Db::name('user')->where('openId', $b)->find();
    $tid  = input('tid', '', 'trim');
    $type  = input('type', '', 'trim');
    $time  = date('y-m-d h:i:s',time());
    $t  = date('m月d日h:i',time());
    $y  = date('y',time());
    $m  = date('m',time());
    $d  = date('d',time());
    $h = date('H:i',time());
    $topicid =  Db::name('topics')->where('id', $tid)->field('topic')->find();
    if($topicid['topic']){
            Db::name('topic')->where('_id', $topicid['topic'])->setInc('hot',1);
    }




    $data = ['a' =>$a,'b' =>$b,'time' =>$time,'tid' =>$tid,'type' =>$type,'aName'=>$aName['userName'],'bName'=>$bName['userName'],'aImg'=>$aName['img'],'bImg'=>$bName['img'],'t' =>$t,'y' =>$y,'m' =>$m,'d' =>$d,'h' =>$h];
     $id=Db::name('action')->insertGetId($data);
        ajax_return_ok($id,1);

    }

    public function addComNoNum(){

        $a  = input('a', '', 'trim');
        $b  = input('b', '', 'trim');
        $code  = input('code', '', 'trim');
        $txt  = input('txt', '', 'trim');
        $type  = input('type', '', 'trim');
        $time  = date('y-m-d h:i:s',time());
        $t  = date('m月d日 h:i',time());
        $y  = date('y',time());
        $m  = date('m',time());
        $d  = date('d',time());
        $h = date('H:i',time());

        $data = ['c'=>0,'d'=>0,'last' =>$txt,"time"=>date('y-m-d-H-i-s',time())];
        Db::name('room')->where("key",$code)->update($data);
        $data = ['a' =>$a,'b' =>$b,'time' =>$time,'txt' =>$txt,'code' =>$code,'type' =>$type,'t' =>$t,'y' =>$y,'m' =>$m,'d' =>$d,'h' =>$h];
       $is= Db::name('com')->insert($data);

       ajax_return_ok($is,"success");

    }

    public function addCom(){

        $a  = input('a', '', 'trim');
        $b  = input('b', '', 'trim');
        $code  = input('code', '', 'trim');
        $txt  = input('txt', '', 'trim');
        $type  = input('type', '', 'trim');
        $time  = date('y-m-d h:i:s',time());
        $t  = date('m月d日 h:i',time());
        $y  = date('y',time());
        $m  = date('m',time());
        $d  = date('d',time());
        $h = date('H:i',time());

        $data = ['last' =>$txt,"time"=>date('y-m-d-H-i-s',time())];
        Db::name('room')->where("key",$code)->update($data);

        $out  = Db::name('room')->where(["a"=>$a,"b" => $b,"key" => $code])->find();
        if($out){
            Db::name('room')->where('key', $code)->setInc('c',1);
             Db::name('room')->where('key', $code)->update(['aa'=>1]);
        }else if($out = Db::name('room')->where(["a"=>$b,"b" => $a,"key" => $code])->find()){
            Db::name('room')->where('key', $code)->setInc('d',1);
             Db::name('room')->where('key', $code)->update(['bb'=>1]);
        }else{

        }
        $data = ['a' =>$a,'b' =>$b,'time' =>$time,'txt' =>$txt,'code' =>$code,'type' =>$type,'t' =>$t,'y' =>$y,'m' =>$m,'d' =>$d,'h' =>$h];
       $is= Db::name('com')->insert($data);

       ajax_return_ok($is,"success");

    }

    public function change(){
        $t  = date('m月d日h:i',time());
        $y  = date('y',time());
        $m  = date('m',time());
        $d  = date('d',time());
        $h = date('H:i',time());
        $a  = input('a', '', 'trim');
        $b  = input('b', '', 'trim');
        $aName  = Db::name('user')->where('openId', $a)->field('userName')->find();
        $bName  = Db::name('user')->where('openId', $b)->field('userName')->find();
        $tid  = input('tid', '', 'trim');
        $type  = 2;
        $time  = date('y-m-d h:i:s',time());
        $data = ['a' =>$a,'b' =>$b,'time' =>$time,'tid' =>$tid,'type' =>$type,'aName'=>$aName['userName'],'bName'=>$bName['userName'],'t' =>$t,'y' =>$y,'m' =>$m,'d' =>$d,'h' =>$h];

        if( !Db::name('action')->where(['tid'=> $tid ,'type' => $type,'a' => $a,'b' => $b])->find()){
            Db::name('topics')->where('id', $tid)->setInc('parise_number',1);
            ajax_return_ok(Db::name('action')->insert($data));
        }else{
            Db::name('topics')->where('id', $tid)->setDec('parise_number',1);
            ajax_return_ok(Db::name('action')->where(['tid'=> $tid ,'type' => $type,'a' => $a,'b' => $b])->delete());
        }



    }

    public function zan(){

        $t  = date('m月d日h:i',time());
        $y  = date('y',time());
        $m  = date('m',time());
        $d  = date('d',time());
        $h = date('H:i',time());

        $a  = input('a', '', 'trim');
        $b  = input('b', '', 'trim');
        $aName  = Db::name('user')->where('openId', $a)->find();
        $bName  = Db::name('user')->where('openId', $b)->find();
        $tid  = input('tid', '', 'trim');
        $code  = input('code', '', 'trim');
        $type  = 2;
        $time  = date('y-m-d h:i:s',time());
        $data = ['a' =>$a,'b' =>$b,'time' =>$time,'tid' =>$tid,'type' =>$type,'aName'=>$aName['userName'],'bName'=>$bName['userName'],'code'=>$code,'t' =>$t,'y' =>$y,'m' =>$m,'d' =>$d,'h' =>$h,'aImg'=>$aName['img'],'bImg'=>$bName['img']];

        if(!Db::name('action')->where(['tid'=> $tid ,'type' => $type,'a' => $a,'b' => $b])->find()){
            Db::name('topics')->where('id', $tid)->setInc('parise_number',1);
          
            $id=Db::name('action')->insertGetId($data);
            ajax_return_ok($this->CODE($tid),$id);
        }else{
            Db::name('topics')->where('id', $tid)->setDec('parise_number',1);
            Db::name('action')->where(['tid'=> $tid ,'type' => $type,'a' => $a,'b' => $b])->delete();
            ajax_return_ok($this->CODE($tid),0);
        }

            

    }
  
    public  function CODE($tid){
        $OUT = \app\admin\model\Topics::with('action,user,img,topic,like')->order('created_at desc')->where('id',$tid)->find();//9.13修改select为find

        $A = $OUT;
        foreach ($OUT as $key => $item) {
          
            if(count($A[$key]['action'])){
                $info = $A[$key]['action'];//定义一个中间变量，操作这个变量，结果赋值给$list[$key]['info']
                foreach($info as $k =>$i){
                    $txt =base64_decode($i["txt"]);
                    $info[$k]["txt"] = $txt;
                }
                $A[$key]['action'] =$info;
            }

        }
        return $A;
    }

    public function addComment(){

        $a  = input('a', '', 'trim');
        $b  = input('b', '', 'trim');
        $aName  = Db::name('user')->where('openId', $a)->find();
        $bName  = Db::name('user')->where('openId', $b)->find();
        $tid  = input('tid', '', 'trim');
        $type  = input('type', '', 'trim');
        $txt = input('txt', '', 'trim');
        $time  = date('y-m-d h:i:s',time());
        $code  = input('code', '', 'trim');
        $who  = input('who', '', 'trim');
        $txt1 = input('txt', '', 'trim');
        $topicid =  Db::name('topics')->where('id', $tid)->field('topic')->find();

        if($topicid['topic']){
            Db::name('topic')->where('_id', $topicid['topic'])->setInc('hot',1);

        }
        $t  = date('m月d日h:i',time());
        $y  = date('y',time());
        $m  = date('m',time());
        $d  = date('d',time());
        $h = date('H:i',time());
        $data = ['a' =>$a,'b' =>$b,'time' =>$time,'tid' =>$tid,'type' =>$type,'aName'=>$aName['userName'],'bName'=>$bName['userName'],'txt'=>$txt,'aImg'=>$aName['img'],'bImg'=>$bName['img'],'code'=>$code,'who'=>$who,'t' =>$t,'y' =>$y,'m' =>$m,'d' =>$d,'h' =>$h];

        if(!$this->textVerify($txt1)){
           
            $id=Db::name('action')->insertGetId($data);
            Db::name('topics')->where('id', $tid)->setInc('comment_number',1);
            if($type == "4"||$type == "5"){
                Db::name('action')->where('code', $code)->setInc('repay',1);
            }
            ajax_return_ok($this->CODE($tid),$id);
        }else{
            ajax_return_ok(0,0);
        }



    }

    public function getAction(){
    $id  = input('id', '', 'trim');
    $OUT = \app\admin\model\Action::with('usera')->where('b',$id)->select();
    $A = $OUT;
    foreach ($OUT as $key => $item) {
        

    }
    ajax_return_ok($A);

    }

    public function getComByCode(){
        $code  = input('code', '', 'trim');
        $a  = input('a', '', 'trim');
        $b  = input('b', '', 'trim');
        $N = 0;
        $OUT = [];
        $out  = Db::name('room')->where(["key" => $code])->find();
        if(($out['a'] == $a)&&($out['b'] == $b)){
            $N = $out["c"];
            if($N){
                $OUT = Com::with('usera,userb')->where(["a"=>$a,"b" => $b,"code" => $code])->order('id desc')->limit($N)->select();
            }
            $A = $OUT;
            $data = ['c' =>0];
            Db::name('room')->where(["a"=>$a,"b" => $b])->update($data);
            ajax_return_ok($A);
        }else if(($out['a'] == $b)&&($out['b'] == $a)){
            $N = $out["d"];
            if($N){
                $OUT = Com::with('usera,userb')->where(["a"=>$a,"b" => $b,"code" => $code])->order('id desc')->limit($N)->select();
            }
            $A = $OUT;
            $data = ['d' =>0];
            Db::name('room')->where(["a"=>$b,"b" => $a])->update($data);

            ajax_return_ok($A);

        }



    }
  
    public function  getComAllByCode(){
       $code  = input('code', '', 'trim');
       $openid  = input('openid', '', 'trim');
       $out=Db::name('com')->where('code',$code)->order('time asc')->select();
       $room = Db::name('room')->where('key',$code)->find();
           if($openid==$room['a']){
            Db::name('room')->where('key',$code)->update(['c'=>0]);
        }else{
            Db::name('room')->where('key',$code)->update(['d'=>0]);
        }
  
        
         ajax_return_ok($out);
   }
 
    public function getComByAB(){
        $a  = input('a', '', 'trim');
        $b  = input('b', '', 'trim');
        $OUT = Com::with('usera,userb')->where(['a'=>$a,'b'=>$b])->select();
        $A = $OUT;
//        foreach ($OUT as $key => $item) {
//            $A[$key]['content'] = base64_decode($item['content']);
//
//        }
        ajax_return_ok($A);

    }

    public function getMessageByid(){
        
        $actionId  = input('Actionid', '', 'trim');
        
         $out = \app\admin\model\Action::with('usera,topics')->where('id','=',$actionId)->find();
          Db::name('action')->where('id', $actionId)->update(['status' => 1]);
         if($out){
               ajax_return_ok( $out, "success");
         }else{
              ajax_return_ok( 0, "fail");
         }
        
    }

    public function getMessage(){
        
        $id  = input('id', '', 'trim');
        $whereArr['a'] =  array('neq',$id); // 不等于条件
        $whereArr['b'] = array('eq',$id); // 其他条件
        $whereArr['status'] = array('eq',0); // 其他条件
        $out = \app\admin\model\Action::with('usera,topics')->where($whereArr)->order('time desc')->select();
      //  $A = $out;
//         foreach ($out as $key => $item) {
// //            $A[$key]['content'] = base64_decode($item['content']);
//             if(count($A[$key]['topics'])){
//                 $info = $A[$key]['topics'];//定义一个中间变量，操作这个变量，结果赋值给$list[$key]['info']
//                 foreach($info as $k =>$i){
// //                    $U = \app\admin\model\Topics::with('user')->where("openId",$i["openid"])->find();
// //                    array_push($info[$k]["type"],$U["openId"] );
//                     $txt =$i["content"];
//                     $info[$k]["content"] = $txt;
//                     $info[$k]["img"] =  Db::name('img')->where('tid', $i["id"])->select();
//                 }
//                 $A[$key]['topics'] =$info;
//             }
//         }
        Db::name('action')->where('b', $id)->update(['status' => 1]);
        ajax_return_ok( $out, "success");

    }


    public function getA(){

        ajax_return_ok( $this ->nonceStr());

    }

    public function aa(){
        $txt = input('txt', '', 'trim');;
        $data = ['txt'=>$txt];
        Db::name('action')->insert($data);

    }
 
    public function nonceStr() {
        static $seed = array(0,1,2,3,4,5,6,7,8,9);
        $str = '';
        for($i=0;$i<10;$i++) {
            $rand = rand(0,count($seed)-1);
            $temp = $seed[$rand];
            $str .= $temp;
            unset($seed[$rand]);
            $seed = array_values($seed);
        }
        return $str;
    }
   
    public function getPass(){
        $OUT = \app\admin\model\Topics::with('user,img,topic')->where('status',0)->limit(10)->order('created_at desc')->select();

        $A = $OUT;
        foreach ($OUT as $key => $item) {
           


        }

        ajax_return_ok($A);
    }

    public function deleteById(){
        $id = input('id', '', 'trim');      //3级
        $code = input('code', '', 'trim');
        $who = input('who', '', 'trim');    //2级

        if($id  != ''){
            $tid = Db::name('action')->where('id',$id)->find();
            $n = Db::name('action')->where('id',$id)->delete();
            Db::name('topics')->where('id', $tid["tid"])->setDec('comment_number',$n);
            Db::name('action')->where('code', $tid['code'])->setDec('repay',$n);
            ajax_return_ok( $this->CODE($tid["tid"]));
        }else if($code != ''){
            $tid = Db::name('action')->where('code',$code)->find();
            $n = Db::name('action')->where('code',$code)->delete();
            Db::name('topics')->where('id',$tid["tid"])->setDec('comment_number',$n);

            Db::name('action')->where('code', $code)->setDec('repay',$n);

            ajax_return_ok($this->CODE($tid["tid"]));
        }else{
            $tid = Db::name('action')->where('who',$who)->find();
            $n = Db::name('action')->where('who',$who)->delete();
            Db::name('topics')->where('id', $tid["tid"])->setDec('comment_number',$n);


            ajax_return_ok($this->CODE($tid["tid"]));
        }

    }

    public function getActionByCode(){
        $code  = input('code', '', 'trim');
        $OUT = \app\admin\model\Action::with('usera,userb')->where('code',$code)->order('time desc')->select();
        $A = $OUT;

        ajax_return_ok($A);

    }

    public function  ban(){
        $id = input('id', '', 'trim');
        ajax_return_ok(Db::name('topics')->where('id', $id)->update(['status' => 1]));
    }

    public function  pass(){
        $id = input('id', '', 'trim');
        ajax_return_ok(Db::name('topics')->where('id', $id)->update(['status' => 2]));
    }

    public function textVerify($T){
        $APP_ID = '21511921';
        $API_KEY = 'oAsWGM4sQmX6qH3KFNfVvTWI';
        $SECRET_KEY = 'dN54LZg1kuUle5wdvwdY91prQNeSE8fY';

        $client = new AipImageCensor($APP_ID, $API_KEY, $SECRET_KEY);
        if( $client->textCensorUserDefined($T)["conclusion"] == "合规"){
            return false;
        }else{
            return true;
        }


    }



}