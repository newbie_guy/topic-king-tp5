<?php


namespace app\admin\controller;
use \think\Db;

class Topic
{
    public function addTopicClass(){
        $txt  = input('topic', '', 'trim');
        $openid  = input('openid', '', 'trim');
        $time  = date('y-m-d h:i:s',time());
        if(!Db::name('topic')->where('topic',$txt)->find()){

            $data = ['openid' =>$openid,'time' =>$time,'topic' =>$txt];

            ajax_return_ok( Db::name('topic')->insertGetId($data));
        }else{
            ajax_return_ok("haved");
        }
    }

    public function getAll(){
    ajax_return_ok(\app\admin\model\Topic::with('user')->select());
    }

    public function getHotAll(){
        ajax_return_ok(\app\admin\model\Topic::with('user')->order('hot desc')->limit(8)->select());
    }


    public function deleteById(){
        $id = input('id', '', 'trim');
        Db::name('topics')->where('topic',$id)->delete();
        ajax_return_ok(Db::name('topic')->where('_id',$id)->delete());
    }

    public function getById(){
        $id = input('id', '', 'trim');
        ajax_return_ok(\app\admin\model\Topic::with('user')->where('_id',$id)->find());
    }



    public function getByOpenId(){
        $id = input('id', '', 'trim');
        ajax_return_ok(\app\admin\model\Topic::with('user')->where('openid',$id)->select());
    }



}