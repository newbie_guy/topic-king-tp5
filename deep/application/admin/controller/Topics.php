<?php


namespace app\admin\controller;

use \think\Db;
use AipImageCensor;
use think\session\driver\Redis;
require_once 'AipImageCensor.php';
class Topics
{
    
   public $redis;
    
    public function __construct(){
        // $this->redis = new \Redis();
        // $this->redis->connect('127.0.0.1',6379);
    } 
    //  public function index()
    // {
    //     $redis = $this->redis;
    //     echo $redis->ping();
    // }
    public function addTopics(){

        $openid  = input('openid', '', 'trim');
        $mode  = input('mode', '', 'int');
        $tid  = input('tid', '', 'trim');
        $topicid  = input('topic', '', 'trim');
        $content  = input('content', '', 'trim');
        $time  = date('y-m-d H:i:s',time());
        $t  = date('m月d日 h:i',time());
        $y  = date('y',time());
        $m  = date('m',time());
        $d  = date('d',time());
        $h = date('H:i',time());
        $txt = input('content', '', 'trim');
//        内容判断是否合规
        if($this->textVerify($txt)){
            if($topicid){
                Db::name('topic')->where('_id', $topicid)->setInc('hot',1);
                Db::name('topic')->where('_id', $topicid)->setInc('join',1);
            }
            if(!$mode){
                $data = ['openid' =>$openid,'id' =>$tid,'created_at' =>$time,'topic' =>$topicid,'content' =>$content,'t' =>$t,'y' =>$y,'m' =>$m,'d' =>$d,'h' =>$h,"status" => 2];
            }else{
//                加入数量
                Db::name('mode')->where('mode', $mode)->setInc('join',1);
                $data = ['openid' =>$openid,'id' =>$tid,'created_at' =>$time,'topic' =>$topicid,'content' =>$content,'t' =>$t,'y' =>$y,'m' =>$m,'d' =>$d,'h' =>$h,'mode' =>$mode,"status" => 2];
            }

          $i=Db::name('topics')->insert($data);
          $data = \app\admin\model\Topics::with('action,user,img,topic,like,mode')->where('id',$tid)->find();
          $now = $this->redis->get('now_topics_num');
          $now +=1;
          $this->redis->set('now_topics_num',$now);
          $this->redis->set('topics_'.$now,json_encode($data));
          //$tid
            ajax_return_ok($i,"success");
        }else{
            ajax_return_ok(0,"fail");
        }


    }
     
     public function toRedis (){
          $this->redis->set('now_topics_num',0);
          $OUT = \app\admin\model\Topics::with('action,user,img,topic,like,mode')->where('status=2')->where("top",0)->order('created_at ase')->select();
         $num = count($OUT);
         
         for($i=0;$i<$num;$i++){
              $topic_num =  $this->redis->get('now_topics_num');
              $topic_num +=1;
              $item = json_encode($OUT[$i]);
              $this->redis->set('topics_'.$topic_num,$item);
              $this->redis->set('now_topics_num',$topic_num);
         }
             ajax_return_ok('ok');
     }
    
    public function getAll(){
        $OUT = \app\admin\model\Topics::with('action,user,img,topic,like,mode')->where('status=2')->where("top",0)->order('created_at desc')->paginate(10);
     // $num =  $this->redis->get('now_topics_num');
    //   $this->redis->set('now_topics_num',1);
        // if(!$redis->has('str')){
        //     var_dump($redis->set('str','this is redis_str'));
        // }else{
        //     var_dump($redis->get('str'));
        // }
        ajax_return_ok($OUT,"num");
    }
     public function getAll_R(){
         $OUT = array();
         $topic_num =  $this->redis->get('now_topics_num');
         $i = 0;
         while($i<10){
             $item =  $this->redis->get('topics_'.$topic_num);
             $topic_num -=1;
               if($item){
                   $i++;
                array_push($OUT,$item);     
             }
         }

        // if(!$redis->has('str')){
        //     var_dump($redis->set('str','this is redis_str'));
        // }else{
        //     var_dump($redis->get('str'));
        // }
        ajax_return_ok($OUT,'success');
    }


    public function getHotAll(){
        $OUT = \app\admin\model\Topics::with('action,user,img,topic,like,mode')->where('status=2')->where("top",0)->order('view_number desc')->paginate(10);
        ajax_return_ok($OUT);
    }

    public function getHotAllMore(){
        $page  = input('page', '', 'int');
        $OUT = \app\admin\model\Topics::with('action,user,img,topic,like,mode')->limit(($page-1)*10 ,10)->where('status=2')->where("top",0)->order('view_number desc')->select();
        ajax_return_ok($OUT);
    }

    public function getTop(){
        $OUT = \app\admin\model\Topics::with('action,user,img,topic,like')->where('status=2')->where("top",1)->order('created_at desc')->paginate(10);
        ajax_return_ok( $OUT);
    }

    public function getAllMore(){
        $page  = input('page', '', 'int');

        $OUT = \app\admin\model\Topics::with('action,user,img,topic,like,mode')->limit(($page-1)*10 ,10)->where('status=2')->where("top",0)->order('created_at desc')->select();
        ajax_return_ok($OUT);
    }

    public function getAllByOpenid(){
        $id  = input('id', '', 'trim');
        $A = \app\admin\model\Topics::with('action,user,img,topic,like,mode')->where('openid',$id)->where('status=2')->order('created_at desc')->paginate(10);
        $B = Db::name('user')->where('openId',$id)->find();
        ajax_return_ok(compact('A','B'));
    }

    public function getAllByOpenidMore(){
        $id  = input('id', '', 'trim');
        $page  = input('page', '', 'int');
        $A = \app\admin\model\Topics::with('action,user,img,topic,like,mode')->where('openid',$id)->limit(($page-1)*10 ,10)->where('status=2')->order('created_at desc')->select();
        $B = Db::name('user')->where('openId',$id)->find();
        ajax_return_ok(compact('A','B'));
    }

    public function getAllLikeByOpenid(){

        $id  = input('openid', '', 'trim');
        $list = \app\admin\model\Like::with('userb')->where('a',$id)->select();
        $C = [];
        $time  = date('y-m-d H:i:s',strtotime("-1 day"));
//        exit( $time > date('y-m-d H:i:s',time()));
        $OUT = [];
        foreach($list as $k =>$i){
            $openid =$i["userb"][0]["openId"];

            $T = \app\admin\model\Topics::with('action,user,img,topic,like,mode')->where('created_at','>',$time)->where('openid',$openid)->where('status=2')->order('created_at desc')->select();
            array_push($OUT,$T);
        }

//       ajax_return_ok($OUT);

        foreach ($OUT as $x => $y) {
            $A = $y;
            foreach ($y as $key => $item) {

                array_push($C,$A[$key]);
            }

        }

        $total = count($C);
        $num =  (int)(count($C)%10>0?count($C)/10+1:count($C)/10);
        $data = array_slice($C,0,10);
//        $B = Db::name('user')->where('openId',$id)->find();
        ajax_return_ok( compact('total','data','num'));


    }

    public function getAllLikeByOpenidMore(){
        $page  = input('page', '', 'int');

        $id  = input('openid', '', 'trim');
        $list = \app\admin\model\Like::with('userb')->where('a',$id)->select();
        $C = [];
        $time  = date('y-m-d H:i:s',strtotime("-1 day"));
//        exit( $time > date('y-m-d H:i:s',time()));
        $OUT = [];
        foreach($list as $k =>$i){
            $openid =$i["userb"][0]["openId"];

            $T = \app\admin\model\Topics::with('action,user,img,topic,like')->where('created_at','>',$time)->where('openid',$openid)->where('status=2')->order('created_at desc')->select();
            array_push($OUT,$T);
        }

//       ajax_return_ok($OUT);

        foreach ($OUT as $x => $y) {
            $A = $y;
            foreach ($y as $key => $item) {


                array_push($C,$A[$key]);
            }

        }

        $total = count($C);
        $num =  count($C)/10;
        $data = array_slice($C,0,10);
//        $B = Db::name('user')->where('openId',$id)->find();
        ajax_return_ok( compact('total','data','num'));

    }

    public function getTopicsByTid(){
        $id  = input('id', '', 'trim');
        $OUT = \app\admin\model\Topics::with('action,user,img,topic,like')->where('id',$id)->order('created_at desc')->select();
        ajax_return_ok($OUT);

    }

    public function getTopicsByTopic(){
        $id  = input('id', '', 'trim');
        $OUT = \app\admin\model\Topics::with('action,user,img,topic,like')->where('topic',$id)->order('created_at desc')->paginate(5);
        ajax_return_ok($OUT);

    }

    public function getTopicsByTopicMore(){
        $page  = input('page', '', 'int');
        $id  = input('id', '', 'trim');
        $OUT = \app\admin\model\Topics::with('action,user,img,topic,like')->where('topic',$id)->limit(($page-1)*5 ,5)->order('created_at desc')->select();
        ajax_return_ok($OUT);
    }

    public function getTopics(){
        $id  = input('id', '', 'trim');
//        $temp =  Db::name('topics')->where('id', $id)->find();
//        Db::name('topic')->where('id', $temp["topic"])->setInc('hot',1);
        $OUT = \app\admin\model\Topics::with('action,user,img,topic,like')->where('id',$id)->order('created_at desc')->select();
        ajax_return_ok($OUT);
    }

    public function getTopicsPlus(){
        $id  = input('id', '', 'trim');
        Db::name('topics')->where('id', $id)->setInc('view_number',1);

        $OUT = \app\admin\model\Topics::with('action,user,img,topic,like')->where('id',$id)->order('created_at desc')->select();
        ajax_return_ok($OUT);
    }

    public function deleteById(){
    $id = input('id', '', 'trim');
    $mode = Db::name('topics')->where('id',$id)->find();
    if($mode["mode"]){
        Db::name('mode')->where('mode', $mode["mode"])->setDec('join',1);
    }
    ajax_return_ok(Db::name('topics')->where('id',$id)->delete());

    }
   
   public function text(){
       
        $id = input('txt', '', 'trim');
       $res=  $this->textVerify($id);
       ajax_return_ok($res,"结果");
   }

    public function textVerify($T)

    {
        $APP_ID = '21511921';
        $API_KEY = 'oAsWGM4sQmX6qH3KFNfVvTWI';
        $SECRET_KEY = 'dN54LZg1kuUle5wdvwdY91prQNeSE8fY';

        $client = new AipImageCensor($APP_ID, $API_KEY, $SECRET_KEY);
        
       // return $client->textCensorUserDefined($T)["conclusion"] ;
        if( $client->textCensorUserDefined($T)["conclusion"] == "合规"||$client->textCensorUserDefined($T)["conclusion"] == "疑似"){
            return true;
        }else{
            return false;
        }


    }

    //图片审核

//    public function imgVerify($img)
//
//    {
//
//        $redis = new Redis();
//
//        $token = $redis->get("filterToken");
//
//        if (empty($token)) {
//
//            $token = $this->getToken();
//
//        }
//
//        $curl = $this->imgUrl . "?access_token=" . $token;
//
//        $bodys = array(
//
//            'image' => $img,
//
//            'scenes' => array("ocr",
//
//                "face", "public", "politician", "antiporn", "terror", "webimage", "disgust",
//
//                'watermark')
//
//        );
//
//        $bodys = json_encode($bodys);
//
//        $result = self::request_post($curl, $bodys, "img");
//
//        return json_decode($result, true);
//
//    }


}