<?php


namespace app\admin\controller;
use \think\Db;

class Wxlogin
{
    public function vget($url){
        $info=curl_init();
        curl_setopt($info,CURLOPT_RETURNTRANSFER,true);
        curl_setopt($info,CURLOPT_HEADER,0);
        curl_setopt($info,CURLOPT_NOBODY,0);
        curl_setopt($info,CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($info,CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($info,CURLOPT_URL,$url);
        $output= curl_exec($info);
        curl_close($info);
        return $output;
    }

    public function index(){
        //开发者使用登陆凭证 code 获取 session_key 和 openid

        $APPID = "wx39108640d223c6a4";//自己配置
        $AppSecret = "6345ae7a52188e0b4c03b5caac9ba7f1";//自己配置
        $code = input('code', '', 'trim');
        $name = input('name', '', 'trim');
        
        $province=input('province','','trim');
         $city=input('city','','trim');
        $tu = input('url', '', 'trim');
        $sex = input('sex', '', 'int');
        $url = "https://api.weixin.qq.com/sns/jscode2session?appid=" . $APPID . "&secret=" . $AppSecret . "&js_code=" . $code . "&grant_type=authorization_code";
        $arr = $this->vget($url);  // 一个使用curl实现的get方法请求
        $arr = json_decode($arr, true);
        $openid = $arr['openid'];
        $this->add($openid,$name,$tu,$sex);
        $this->addTao($openid,$name,$tu,$sex,$province,$city);
       exit($openid);
    //  ajax_return_ok($rawData,'err'); 

    }
        public function login(){
        //开发者使用登陆凭证 code 获取 session_key 和 openid

        $APPID = "wx39108640d223c6a4";//自己配置
        $AppSecret = "6345ae7a52188e0b4c03b5caac9ba7f1";//自己配置
        $code = input('code', '', 'trim');
     
        $url = "https://api.weixin.qq.com/sns/jscode2session?appid=" . $APPID . "&secret=" . $AppSecret . "&js_code=" . $code . "&grant_type=authorization_code";
        $arr = $this->vget($url);  // 一个使用curl实现的get方法请求
        $arr = json_decode($arr, true);
        ajax_return_ok($arr);
        //exit($arr);

    }    
    public function addTao($id,$name,$url,$sex,$city,$province)
    {  
        $db=Db::connect('tao');
        if(!$db->name('wx_user')->where('openid',$id)->find()){
            $data = [
            'openid' =>$id,
            'create_time' =>date('y-m-d H:i:s',time()),
            'nick_name' =>$name,
            'avatar_url' =>$url,
            'gender' => $sex+0,
            'province'=>$province,
            'city'=>$city
            ];
          $db->name('wx_user')->insert($data);
        }else{
            $db->name('wx_user')->where('openid', $id)->update(['last_time' => date('y-m-d h:i:s',time())]);
        }

    }

    public function add($id,$name,$url,$sex)
    {
        if(!Db::name('user')->where('openId',$id)->find()){
            $data = ['openId' =>$id,'regTime' =>date('y-m-d H:i:s',time()),'userName' =>$name,'img' =>$url,'sex' => $sex+0];
            Db::name('user')->insert($data);
        }else{
            Db::name('user')->where('openId', $id)->update(['loginTime' => date('y-m-d h:i:s',time())]);
        }

    }
}