<?php


namespace app\admin\controller;

use \think\Db;
class Mode
{
        public  function getAllMode(){

            ajax_return_ok(Db::name("mode")->where("ban",0)->select());
        }

        public  function getAllTopics(){
            $mode  = input('mode', '', 'int');

            $OUT = \app\admin\model\Topics::with('action,user,img,topic,like,mode')->where("mode",$mode)->where('status=2')->where("top",0)->order('created_at desc')->paginate(10);

            $A = $OUT;
            // foreach ($OUT as $key => $item) {
            //     $A[$key]['content'] = base64_decode($item['content']);
            //     if(count($A[$key]['action'])){
            //         $info = $A[$key]['action'];//定义一个中间变量，操作这个变量，结果赋值给$list[$key]['info']
            //         foreach($info as $k =>$i){
            //             $txt =base64_decode($i["txt"]);
            //             $info[$k]["txt"] = $txt;
            //         }
            //         $A[$key]['action'] =$info;
            //     }

            // }



          ajax_return_ok($A);


        }

    public  function getOrder(){
        $mode  = input('mode', '', 'int');

        $OUT = \app\admin\model\Topics::with('action,user,img,topic,like,mode')->where('status=2')->where("top",0)->where("mode",$mode)->order('parise_number desc')->limit(10)->select();

        $A = $OUT;
        foreach ($OUT as $key => $item) {
            $A[$key]['content'] = base64_decode($item['content']);
            if(count($A[$key]['action'])){
                $info = $A[$key]['action'];//定义一个中间变量，操作这个变量，结果赋值给$list[$key]['info']
                foreach($info as $k =>$i){
                    $txt =base64_decode($i["txt"]);
                    $info[$k]["txt"] = $txt;
                }
                $A[$key]['action'] =$info;
            }

        }

        ajax_return_ok($A);


    }
 public function getAllTopicsMore(){
        $page  = input('page', '', 'int');
        $mode  = input('mode','','int');
        
        $OUT = \app\admin\model\Topics::with('action,user,img,topic,like,mode')->limit(($page-1)*10 ,10)->where('status',2)->where("top",0)->where("mode",$mode)->order('created_at desc')->select();
         
        $A = $OUT;
        foreach ($OUT as $key => $item) {
            $A[$key]['content'] = base64_decode($item['content']);
            if(count($A[$key]['action'])){
                $info = $A[$key]['action'];//定义一个中间变量，操作这个变量，结果赋值给$list[$key]['info']
                foreach($info as $k =>$i){
                    $txt =base64_decode($i["txt"]);
                    $info[$k]["txt"] = $txt;
                }
                $A[$key]['action'] =$info;
            }

        }

        ajax_return_ok($A);
    }

}