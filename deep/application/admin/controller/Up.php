<?php


namespace app\admin\controller;
use \think\Db;
Vendor('qiniu.php-sdk.autoload');
use think\Config;
use think\Image;
use think\Request;
use Qiniu\Auth as Auth;
use Qiniu\Storage\BucketManager;
use Qiniu\Storage\UploadManager;
use Qiniu\Http\Client;

class Up
{

    public function uploadimg()
    {
        $tid = input('tid', '', 'trim');
        $file = request()->file('file');
        if ($file) {
            $info = $file->move('public/upload/');
            if ($info) {
                $file = "https://deepzero.top/public/upload/".$info->getSaveName();

                $res = ['errCode' => 0, 'tid' => $tid, 'file' => $file];
                $this->add($tid,$file);
                return json($res);
            }
        }

    }

    public  function add($tid,$src){

        $src = str_replace("\\","/",$src);
        $data = ['tid' =>$tid,'src' =>$src];
        Db::name('img')->insert($data);
    }

    public  function addImg(){
        $tid = input('tid', '', 'trim');
        $src = input('src/a');
        foreach( $src as $k =>$i){
            if($this->juge($i)){
                $data = ['tid' =>$tid,'src' =>$i];
               $res= Db::name('img')->insert($data);
                
            }else{
                $data = ['tid' =>$tid,'src' =>'https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1595323882714&di=4a377a9fde463d25a327d43c9d68bcee&imgtype=0&src=http%3A%2F%2Fbxzphz.cn%2Fuploads%2F1582955021_7955.png'];
                Db::name('img')->insert($data);

            }

        }
         ajax_return_ok($res,"addImg");


    }


    public function test()
     {
         $tid = input('tid', '', 'trim');
         if(request()->isPost()){
             $file = request()->file('file');
             // 要上传图片的本地路径
             $filePath = $file->getRealPath();
             $ext = pathinfo($file->getInfo('name'), PATHINFO_EXTENSION);  //后缀

             // 上传到七牛后保存的文件名
             $key =substr(md5($file->getRealPath()) , 0, 5). date('YmdHis') . rand(0, 9999) . '.' . $ext;

             // 需要填写你的 Access Key 和 Secret Key
             $accessKey = "gEHiywIvuGyftgXXC0D82EILxc73tEWp_MU1A4Y0";
             $secretKey = "EZ39e1QeB2YXuKq_Hx5bBS6GM-gcJNLvn9EWctCB";
             // 构建鉴权对象
            $auth = new Auth($accessKey, $secretKey);
           // 要上传的空间
            $bucket = "nsudongdong";
            $domain = "http://www.deepzero.top";
             $token = $auth->uploadToken($bucket);
            // 初始化 UploadManager 对象并进行文件的上传
            $uploadMgr = new UploadManager();
             // 调用 UploadManager 的 putFile 方法进行文件的上传
             list($ret, $err) = $uploadMgr->putFile($token, $key, $filePath);

             if ($err !== null) {
                 return json(["err"=>1,"msg"=>$err,"data"=>"上传失败"]);
                 die;
             } else {
                 if($this->juge($domain ."/". $ret['key'])){
                     $this->add($tid,$domain ."/". $ret['key']);
                 }else{
                     $this->add($tid,"https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1595323882714&di=4a377a9fde463d25a327d43c9d68bcee&imgtype=0&src=http%3A%2F%2Fbxzphz.cn%2Fuploads%2F1582955021_7955.png");
                 }

                 return  json(["err"=>0,"msg"=>"上传完成","data"=>$domain ."/". $ret['key']]);
                 die;
           }
        }
         return  json(["err"=>0,"msg"=>"上传完成"]);
     }

     public function juge($img){

         $accessKey = 'gEHiywIvuGyftgXXC0D82EILxc73tEWp_MU1A4Y0';
         $secretKey = 'EZ39e1QeB2YXuKq_Hx5bBS6GM-gcJNLvn9EWctCB';
         $auth = new Auth($accessKey, $secretKey);

         $url = "http://ai.qiniuapi.com/v3/image/censor";
         $method = "POST";
         $host = "ai.qiniuapi.com";
         $body = "{ \"data\": { \"uri\": \"".$img ."\" } ,\"params\": {\"scenes\": [\"pulp\",\"terror\",\"ads\",\"politician\"]}}";

         $contentType = "application/json";
         $headers = $auth->authorizationV2($url, $method, $body, $contentType);
         $headers['Content-Type'] = $contentType;
         $headers['Host'] = $host;

         $response = Client::post($url, $body, $headers);

         if ($response->ok()) {
             $r = $response->json();
             if($r['result']['suggestion'] == "review"){

                return false;
             }else{

                return true;
             }

         }
     }

     public function getT(){
         $accessKey = "gEHiywIvuGyftgXXC0D82EILxc73tEWp_MU1A4Y0";
         $secretKey = "EZ39e1QeB2YXuKq_Hx5bBS6GM-gcJNLvn9EWctCB";
         // 构建鉴权对象
         $auth = new Auth($accessKey, $secretKey);
         // 要上传的空间
         $bucket = "nsudongdong";

         $token = $auth->uploadToken($bucket);
         ajax_return_ok( $token);
     }


}