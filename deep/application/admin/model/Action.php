<?php


namespace app\admin\model;
use \think\Model;

class Action extends Model
{
    public function usera(){
        return $this ->hasMany('user','openId','a');
    }
    public function userb(){
        return $this ->hasMany('user','openId','b');
    }

    public function topics()
    {
        return $this->hasMany('topics','id','tid');
    }
}