<?php


namespace app\admin\model;

use \think\Model;
class Topics  extends Model
{
    public function user()
    {
        return $this->hasMany('user','openId','openid');
    }
  public function mode()
    {
        return $this->hasMany('mode','mode','mode');
    }

    public function img()
    {
        return $this->hasMany('img','tid','id');
    }

    public function action()
    {

        return $this->hasMany('action','tid','id')->where('type',['=',"1"],['=',"3"],['=',"2"],'or');
    }
  public function actionall()
    {

        return $this->hasMany('action','tid','id');
    }
    public function action1()
    {

        return $this->hasMany('action','tid','id')->where('type',['=',"4"],['=',"3"],['=',"5"],'or');
    }

    public function topic()
    {
        return $this->hasMany('topic','_id','topic');
    }

    public function like()
    {
        return $this->hasMany('like','b','openid');
    }
}