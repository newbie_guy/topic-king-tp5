<?php


namespace app\admin\model;
use \think\Model;

class Like extends Model
{
    public function usera()
    {
        return $this->hasMany('user','openId','a');
    }

    public function userb()
    {
        return $this->hasMany('user','openId','b');
    }

    public function img()
    {
        return $this->hasMany('img','tid','id');
    }

    public function action()
    {

        return $this->hasMany('action','tid','id');
    }

    public function topic()
    {
        return $this->hasMany('topic','_id','topic');
    }
}