<?php
/**
 * This file is part of workerman.
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the MIT-LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @author walkor<walkor@workerman.net>
 * @copyright walkor<walkor@workerman.net>
 * @link http://www.workerman.net/
 * @license http://www.opensource.org/licenses/mit-license.php MIT License
 */

/**
 * 用于检测业务代码死循环或者长时间阻塞等问题
 * 如果发现业务卡死，可以将下面declare打开（去掉//注释），并执行php start.php reload
 * 然后观察一段时间workerman.log看是否有process_timeout异常
 */
//declare(ticks=1);

/**
 * 聊天主逻辑
 * 主要是处理 onMessage onClose
 */
use \GatewayWorker\Lib\Gateway;

class Events
{
   
   /**
    * 有消息时
    * @param int $client_id
    * @param mixed $message
    */
    public static function onMessage($client_id, $message)
    {
        // debug
        echo "client:{$_SERVER['REMOTE_ADDR']}:{$_SERVER['REMOTE_PORT']} gateway:{$_SERVER['GATEWAY_ADDR']}:{$_SERVER['GATEWAY_PORT']}  client_id:$client_id session:".json_encode($_SESSION)." onMessage:".$message."\n";
        
        // 客户端传递的是json数据
        $message_data = json_decode($message, true);
        $new_message =array('type'=>'tong');
        if (!$message_data) {
            $new_message['return'] = "no data";
            return Gateway::sendToCurrentClient(json_encode($new_message));
        }
        // $new_message = $message_data;
        // $new_message['type'] = 'tong';
        //       Gateway::sendToCurrentClient(json_encode($new_message));
        // 根据类型执行不同的业务
        switch ($message_data['type']) {
            
             
            // 客户端回应服务端的心跳
            case 'pong':
                
                $new_message =[];
                $new_message['ok'] = "dd";
                Gateway::sendToCurrentClient(json_encode($new_message));
                return;
            // 客户端登录 message格式: {type:login, name:xx, room_id:1} ，添加到客户端，广播给所有客户端xx进入聊天室
            case 'login':
               
                return;
                
            // 客户端发言 message: {type:say, to_client_id:xx, content:xx}
            case 'say':
               return;
            case 'logout':
                return;
            case 'all':
                $room_id = $message_data['room_id'];
                // 获取房间内所有用户列表
                $clients_list = Gateway::getClientSessionsByGroup($room_id);
                foreach ($clients_list as $tmp_client_id=>$item) {
                    $clients_list[$tmp_client_id] = $item['client_name'];
                }
                $clients_list[$client_id] = $client_name;
            
                return Gateway::sendToCurrentClient(json_encode($clients_list));
            case 'isonline':
                $room_id = $message_data['room_id'];
                $client_name = $message_data['client_name'];
                $_SESSION['room_id'] = $room_id;
                $_SESSION['client_name'] = $client_name;
				
                //绑定uid
                Gateway::bindUid($client_id, $message_data['openid']);
                $demo = $message_data;
                $demo['type'] = 'sayback';
				$demo['uid'] = Gateway::getClientIdByUid($message_data['openid']);
                Gateway::sendToCurrentClient(json_encode($demo));
                
				// 获取房间内所有用户列表
                $clients_list = Gateway::getClientSessionsByGroup($room_id);
                foreach ($clients_list as $tmp_client_id=>$item) {
                    $clients_list[$tmp_client_id] = $item['client_name'];
                }
                $clients_list[$client_id] = $client_name;
            
                // 转播给当前房间的所有客户端，xx进入聊天室 message {type:login, client_id:xx, name:xx}
                $new_message = array('type'=>'isonline', 'client_id'=>$client_id,'client_openid'=>$client_name, 'time'=>date('Y-m-d H:i:s'));
                Gateway::sendToGroup($room_id, json_encode($new_message));  
				  //加入组
			    Gateway::joinGroup($client_id, $room_id);
                // 给当前用户发送用户列表
                $new_message['client_list'] = $clients_list;
                $new_message['type'] = 'all';
                Gateway::sendToCurrentClient(json_encode($new_message));
                return;
				
                
            case 'sayback':
                return;
				
            case 'sayByopenid':
                
                $new_message =  array(
                'type' => 'sayByopneid',
                'time'=>date('Y-m-d H:i:s'),
                'myopenid' => $message_data['myopenid'],
                'otheropenid' => $message_data['otheropenid'],
                'content' => $message_data['content'],
                'room' => $message_data['room'],
                'room_id' => 'dd5ffff24591c30',
                'client_name' => $message_data['client_name'],
                );
                $_SESSION['room'] = $message_data['room'];
                 Gateway::joinGroup($client_id, $message_data['room']);
                 Gateway::sendToUid($message_data['otheropenid'], json_encode($new_message));
                //test
                $new_message['type'] = 'sayback';
                $new_message['getclientidByuid'] = Gateway::getClientIdByUid($message_data['otheropenid']);
                Gateway::sendToCurrentClient(json_encode($new_message));
                return;
				
            case 'topicMsg':
					
					$new_message =  array(
					'type' => 'topicMsg',
					'time'=>date('Y-m-d H:i:s'),
					'from_openid' => $message_data['from_openid'],
					'to_openid' => $message_data['to_openid'],
					'Actionid' => $message_data['Actionid'],
					'room_id' => $message_data['room_id'],
					);
			 Gateway::sendToUid($message_data['to_openid'], json_encode($new_message));
                return;
			case 'newTopic':
			$new_message = array(
			  'type'=>'newTopic',
			  'msgType'=>'addTopic',
			  'make'=>'refresh'
			);
			   Gateway::sendToAll(json_encode($new_message));
			   return;
        }
    }
   
    /**
     * 当客户端断开连接时
     * @param integer $client_id 客户端id
     */
    public static function onClose($client_id)
    {
        // debug
        echo "client:{$_SERVER['REMOTE_ADDR']}:{$_SERVER['REMOTE_PORT']} gateway:{$_SERVER['GATEWAY_ADDR']}:{$_SERVER['GATEWAY_PORT']}  client_id:$client_id onClose:''\n";
       
        // 从房间的客户端列表中删除
        if (isset($_SESSION['room_id'])) {
            $room_id = $_SESSION['room_id'];
            $room = $_SESSION['room'];
            Gateway::leaveGroup($client_id, $room_id);
            Gateway::ungroup($room);
            $new_message = array(
            'type'=>'logout', 'from_client_id'=>$client_id,
            'from_client_name'=>$_SESSION['client_name'],
            'time'=>date('Y-m-d H:i:s'),
            'from_client_openid' =>$_SESSION['client_name']
            );
            Gateway::sendToAll(json_encode($new_message));
            //Gateway::sendToGroup($room_id, json_encode($new_message));
        }
    }
}
